# Pluggable Transport Specification (Version 1)

## Abstract

Pluggable Transports (PTs) are a generic mechanism for the rapid
development and deployment of censorship circumvention,
based around the idea of modular sub-processes that transform
traffic to defeat censors.

This document specifies the sub-process startup, shutdown,
and inter-process communication mechanisms required to utilize
PTs.
